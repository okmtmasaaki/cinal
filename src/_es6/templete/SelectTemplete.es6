
export default class SelectTemplete{

	//===================================================
	// コンストラクタ
	//===================================================
	constructor(main,sectionName,paramName){
		this.main = main;
		this.pc = main.pc;
		this.ac = main.ac;
		this.el = sectionName;
		this.pn = paramName;
		this.dom = $(this.el).find("select");
		this.dom.change((e) => {this.change(e)});
		this.init = ()=>{this.initialize()}

		//this.change();
		//this.init();
	}
	change(e){
		let param = this.dom.val();
		if(param == "不明") param = "";
		let itemname = this.dom.find("option:selected").text();
		this.ac.changeParam(this.pn,param);
		this.ac.changeItemName(this.pn,itemname);
	}
	reset(){
		this.dom.val("");
	}
	initialize(){
		let oldData = this.ac.getParam(this.pn);
		let newData = this.dom.val();
		if(newData =="不明") newData = "";
		if(oldData == newData) return;
		this.change();
	}
	deleteParam(){
		this.ac.changeParam(this.pn,"");
	}
}
