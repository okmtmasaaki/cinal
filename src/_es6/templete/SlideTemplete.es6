
export default class SlideTemplete{

	//===================================================
	// コンストラクタ
	//===================================================
	constructor(main,sectionName,paramName){
		this.main = main;
		this.pc = main.pc;
		this.ac = main.ac;
		this.el = sectionName;
		this.pn = paramName;
		this.dom = $(this.el);
		this.init = ()=>{this.initialize()}
		//スライドショー
		this.sw = new Swiper(this.el+" .swiper-container",{
			centeredSlides:true,
			slidesPerView: 2,
			spaceBetween:30,
			pagination: {
				el: this.el + ' .swiper-pagination',
				type: 'bullets',
				clickable: true
			},
			navigation: {
				nextEl: this.el + ' .swiper-ui__btn__next',
				prevEl: this.el + ' .swiper-ui__btn__prev'
			}
		});

		//this.sw.on('slideChangeTransitionStart', () => {this.main.isBtnActive = false;});
		this.sw.on('slideChangeTransitionEnd', () => {this.change()});

		this.sw.slideTo(0);
		//this.init();
	}
	//===================================================
	// メソッド
	//===================================================
	change(postflg=true){
		let val = this.dom.find(".swiper-slide-active").find(".value").text();
		let exp = this.dom.find(".swiper-slide-active").find(".exp").html();
		let param = this.dom.find(".swiper-slide-active").data("value");
		let itemname = this.dom.find(".swiper-slide-active").find(".value").text();
		this.dom.find(".page__question__content__selected-text").text(val);
		this.dom.find(".page__question__content__read").html(exp);
		this.ac.changeParam(this.pn,param);
		this.ac.changeItemName(this.pn,itemname);
		//this.main.isBtnActive = true;
		this.illg();
	}

	illg(){}
	reset(){
		this.sw.slideTo(0);
	}
	initialize(){
		let oldData = this.ac.getParam(this.pn);
		let newData = this.dom.find(".swiper-slide-active").data("value");
		if(oldData == newData) return;
		this.change();
	}

	deleteParam(){
		this.ac.changeParam(this.pn,"");
	}

}
