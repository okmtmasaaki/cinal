export default class ImageFill{
	constructor(dom,state="cover"){
		$(dom).each((index, el) => {
			let imgsrc = $(el).find('img').attr("src");
			$(el).find("p").hide();
			$(el).find("img").hide();
			$(el).css({
				"background": 'url('+imgsrc+') center center no-repeat',
				"background-size": state
			});
		});
	}
}

