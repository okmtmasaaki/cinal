

/******************************************************
 *
 * GoogleMapEmbed
 *
 *****************************************************/
export default class GoogleMapEmbed{

    //===================================================
    // コンストラクタ
    //===================================================
    constructor(id,lat,lang,zl) {

		let latlang = new google.maps.LatLng(lat, lang);
		let Options = {
		zoom: zl,      //地図の縮尺値
		center: latlang,    //地図の中心座標
		mapTypeId: 'roadmap'   //地図の種類
		};
		this.map = new google.maps.Map(document.getElementById(id), Options);
		this.addPin(lat,lang);
	}


	//===================================================
	// Method
	//===================================================

	addPin(lat,lang){
		let latlang = new google.maps.LatLng(lat, lang);
		let marker = new google.maps.Marker({ // マーカーの追加
			position: latlang, // マーカーを立てる位置を指定
			map: this.map // マーカーを立てる地図を指定
		});
	}


	//===================================================
	// Event
	//===================================================





}


