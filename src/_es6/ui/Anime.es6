export default class Anime{

    //===================================================
    // コンストラクタ
    //===================================================
    constructor() {
    	this.con = new ScrollMagic.Controller();
	}




	createDefAnime(element){
		TweenMax.set(element,{opacity:0})
		new ScrollMagic.Scene({triggerElement:element,offset:-200,reverse:false})
			.on("enter",()=>{
				TweenMax.to(element,2,{opacity:1,ease:Power2.easeOut})
			})
			.on("leave",()=>{})
			.addTo(this.con);
	}
	createAccAnime(element,direction){
		let defxpos = 0;
		switch(direction){
			case "l":defxpos = -100;break;
			case "r":defxpos = 100;break;
		}

		TweenMax.set(element,{x:defxpos,opacity:0})
		new ScrollMagic.Scene({triggerElement:element,offset:-200,reverse:false})
			.on("enter",()=>{
				TweenMax.to(element,2,{x:0,opacity:1,ease:Power2.easeOut})
			})
			.on("leave",()=>{})
			.addTo(this.con);
	}
	createCustomFunc(propsm,customcss,func1,func2){
		TweenMax.set(propsm.triggerElement,customcss);
		new ScrollMagic.Scene(propsm)
			.on("enter",func1)
			.on("leave",func2)
			.addTo(this.con);
	}
}
