

/****************************************************************
 *
 *  NumUtil [update:2017/9/4]
 *
 ****************************************************************/
var NumUtil = (function() {
    function NumUtil() {}

	//==================================================
	// 文字列を数値に変換
	//==================================================
	NumUtil.toNumber = function(stringNum){	return parseInt(stringNum,10);	};

    //==================================================
    // 数値をカンマ付き文字列に変換
    //==================================================
    NumUtil.convertCommaString = function(num){
        return num.toString().replace(/(\d)(?=(\d{3})+$)/g , '$1,');
    };

    //==================================================
    // 単位付き＆カンマ付き数字を、数値に変換 ( "24px":string → 24:int )
    //==================================================
    NumUtil.convertInt = function(str){
        var out = str;
        if( typeof(str)=="string" ) {
            var str1 = str.replace(/,/g, "");
            var out = parseInt(str1, 10);
        }
        return out;
    };

    //==================================================
    // 足りない桁に0を追加する。文字列化。　( 12:number → 012:string )
    //==================================================
    NumUtil.convertDigitNum = function(num, figures){
        var str = String(num);
        while (str.length < figures) {
          str = "0"+str;
        }
        return str;
    };

    return NumUtil;
})();



/****************************************************************
 *
 *  ArrayUtil [update:2018/1/9]
 *
 ****************************************************************/
var ArrayUtil = (function() {
    function ArrayUtil() {}

	//==================================================
	// オブジェクト内の指定要素のテキストが部分一致する配列を返す
	// http://lifelog.main.jp/wordpress/?p=2557
	//==================================================
	ArrayUtil.getFilteringArray = function( array, key, word ){
		var list = array.filter(function(obj) {
			if(obj[key].indexOf(word) != -1) return true;
		});
		return list;
	};

    //==================================================
    // 配列から重複を取り除いて返す
    // http://qiita.com/cocottejs/items/7afe6d5f27ee7c36c61f
    // http://03log.me/blog/2014-08-29-js-array-remove.html
    //==================================================
    ArrayUtil.convertUniqueArray = function(array){
        var sort = array.filter(function (x, i, self) {
            return self.indexOf(x) === i;
        });
        return sort;
    };

	//==================================================
	// オブジェクト配列から重複を取り除いて返す(filterメソッドに渡すためのfunction)
	// https://qiita.com/NewGyu/items/eb52784e9860d6240765
	// -----------------
	// > datas.filter(ArrayUtil.uniqueObjectArrayFilter(["id"]))
	// > datas.filter(ArrayUtil.uniqueObjectArrayFilter(["name","tag"]))
	//==================================================
    ArrayUtil.uniqueObjectArrayFilter = function(fieldNames){
		var self = this;
		return function(item, i, arr) {
			return i == indexOf(arr, item, equalsAllFields)
		};

		// arrのなかにobjが含まれていればそのインデックス番号を返す
		// 探し方はcomparatorを使って探す
		function indexOf(arr, obj, comparator) {
			for(var index in arr) {
				if(comparator(obj, arr[index]) == true) return index;
			}
			return -1;
		};

		// オブジェクトaとbが fieldNamesに当てられたプロパティーを比較して同じであればtrueを返す
		function equalsAllFields(a, b) {
			for(var i in fieldNames) {
				var f = fieldNames[i];
				if(a[f] !== b[f]) return false;
			}
			return true;
		};
	};

	//==================================================
	// 配列の中から最大値/最小値を返す
	// https://www.softel.co.jp/blogs/tech/archives/1377
	//==================================================
	ArrayUtil.getMaxNum = function(array){
		return Math.max.apply(null, array);
	};
	ArrayUtil.getMinNum = function(array){
		return Math.min.apply(null, array);
	};


    return ArrayUtil;
})();



/****************************************************************
 *
 *  ObjectUtil [update:2017/10/26]
 *
 ****************************************************************/
var ObjectUtil = (function() {
    function ObjectUtil() {}

	//==================================================
	// CSVをJSON形式(Array)に変換する
	// ※半角「,(カンマ)」 を入力する際は、「\,」とバックスラッシュを入力する。
	// ※半角「"」を使えません。「'」か全角「”」なら使える。
	// http://lifelog.main.jp/wordpress/?p=2970
	// http://www.hp-stylelink.com/news/2014/08/20140826.php
	//==================================================
	ObjectUtil.csv2json = function(csvText){
    	var csvTextLF = csvText.replace(/\r\n?/g,"\n");//[!]改行コード「LF」に統一する
		var csvArray = csvTextLF.split('\n'); // 1行ごとに分割する
		var jsonArray = [];

		// 1行目から「項目名」の配列を生成する
		var items = csvArray[0].split(',');

		// CSVデータの配列の各行をループ処理する
		//// 配列の先頭要素(行)は項目名のため処理対象外
		//// 配列の最終要素(行)は空のため処理対象外
		for (var i = 1; i < csvArray.length - 1; i++) {
			var j;
			var a_line = new Object();

			// (1) 一時的に「\,」を「#####」に変換
			var _temp = csvArray[i].replace(/\\,/g,"#####");

			// (2)カンマで区切って配列に分ける
			var _tempArray = _temp.split(',');

			// (3)1で変換した「#####」を「,」に戻す
			// (4)最初の文字が「"」の場合トル、最後の文字が「"」の場合トル
			var csvArrayD = [];
			for(j=0; j<_tempArray.length; j++){
				csvArrayD[j] = _tempArray[j].replace(/#####/g,",");
				if(csvArrayD[j].charAt(0) == "\"")						csvArrayD[j] = csvArrayD[j].slice(1);
				if(csvArrayD[j].charAt(csvArrayD[j].length-1) == "\"")	csvArrayD[j] = csvArrayD[j].slice(0,-1);
			}

			// (5)各データをループ処理する
			for (j = 0; j < items.length; j++) {
				// 要素名：items[j]
				// データ：csvArrayD[j]
				a_line[ items[j].toString() ] = csvArrayD[j];
			}
			jsonArray.push(a_line);
		}
		return jsonArray;
	};

	//==================================================
	// Object がカラかどうか判別する
	// http://saba.omnioo.com/note/990/javascript%E3%81%AE%E7%A9%BA%E3%81%AE%E3%82%AA%E3%83%96%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E3%81%AE%E5%88%A4%E5%AE%9A%E6%96%B9%E6%B3%95/
	//==================================================
	ObjectUtil.isEmpty = function(obj){
		return (Object.keys(obj).length === 0) ? true : false;
	};

    //==================================================
    // Object を新規にコピーして返す
    //==================================================
    ObjectUtil.getCopyObject = function(originalObject){
        return $.extend(true, {}, originalObject);
    };

    //==================================================
    // 配列 を新規にコピーして返す
    //==================================================
    ObjectUtil.getCopyArray = function(originalArray){
        return $.extend(true, [], originalArray);
    };


    return ObjectUtil;
})();



/****************************************************************
 *
 *  BrowserUtil [update:2017/12/11]
 *
 ****************************************************************/
var BrowserUtil = (function() {
    function BrowserUtil() {}


	//==========================================================
	// キャッシュ回避用のパラメーター
	//==========================================================
	BrowserUtil.noCashe = function(){
    	var out = "?" + new Date().getTime();
		return  out;
	};

    //==========================================================
    // ■ URLパラメーターを Object型で返す
    // http://qiita.com/tonkatu_tanaka/items/99d167ded9330dbc4019
    //==========================================================
    BrowserUtil.getParamObjectFromUrl = function(){
        var obj = new Object();
        var pair = location.search.substring(1).split('&');
        for(var i=0;pair[i];i++) {
            var kv = pair[i].split('=');
            obj[kv[0]]=kv[1];
        }
        return obj;
    };

	//==========================================================
	// ■ 現在のURLからhtml名を取得する
	// http://qiita.com/gorton/items/ded2d128ded9c9f732e8
	//==========================================================
	BrowserUtil.getCurrentHtmlID = function(){
		var url = window.location.href;
		var reg = url.match(".+/(.+?)\.[a-z]+([\?#;].*)?$");
		return (reg != null) ? reg[1] : "index";
	};
	BrowserUtil.getCurrentHtmlName = function(){
		var url = window.location.href;
		var reg = url.match(".+/(.+?)([\?#;].*)?$");
		return (reg != null) ? reg[1] : "index.html";
	};

	//==========================================================
	// ■ ページのルートURLを返す
	// https://syncer.jp/javascript-reverse-reference/window-location
	//==========================================================
	BrowserUtil.getRootDomainURL = function(){
		var protocol = window.location.protocol;
		var hostname = window.location.hostname;
		var url = protocol + "//" + hostname + "/";
		return url;
	};

	//==========================================================
    // ■ 画面をロック（スクロール禁止）
    // http://qiita.com/kokushin/items/ef96194bd0fa7c145d7b
    //==========================================================
    BrowserUtil.lockScreen = function(){
        $(window).on('touchmove.noScroll', function(e){ e.preventDefault(); });
        $("body").css({
			"overflow":"hidden",
			"pointer-events":"none"
        });
    };
    BrowserUtil.unlockScreen = function(){
        $(window).off('.noScroll');
        $("body").css({
			"overflow":"visible",
			"pointer-events":"auto"
        });
    };

	//==========================================================
	// ■ 背面DOMを指定してロック（例：dom:"#content" を指定する）
	//==========================================================
	BrowserUtil.lockBgDom = function(dom){
		BrowserUtil._scrolltTop = BrowserUtil.getScrollPosition().top;
		$(dom).css({
			"position":"fixed",
			"top":-BrowserUtil._scrolltTop+"px",
			"pointer-events":"none"
		});
	};
	BrowserUtil.unlockBgDom = function(dom){
		$(dom).css({
			"position":"relative",
			"top":0+"px",
			"pointer-events":"auto"
		});
		window.scrollTo(0, BrowserUtil._scrolltTop);
	};
	BrowserUtil._scrolltTop = 0;

	//==========================================================
	// ■サイト外、もしくはSNS経由でのアクセスかを判別する。
	// https://www.nishishi.com/javascript-tips/doc-referrer.html
	//==========================================================
	BrowserUtil.hasDirectAccess = function(){

		var flag = false;
		var ref = document.referrer;
		var hereHost = window.location.hostname; // 現在ページのホスト(ドメイン)名を得る
		var sStr = "^https?://" + hereHost;
		var rExp = new RegExp( sStr, "i" );

		if( ref.length == 0 ) {				flag = true;
		}else if( ref.match( rExp ) ) {		flag = false;
		}else {								flag = true;	}
		return flag;
	};

    //==========================================================
    // ■主要ブラウザ判定
    // http://qiita.com/Evolutor_web/items/162bfcf83695c83f1077
    //==========================================================
    BrowserUtil.getBrowser = function(){
        var ua = window.navigator.userAgent.toLowerCase();
        var ver = window.navigator.appVersion.toLowerCase();

        var name = 'unknown';
        if (ua.indexOf("msie") != -1){
            if (ver.indexOf("msie 6.") != -1){          name = 'ie';//ie6
            }else if (ver.indexOf("msie 7.") != -1){    name = 'ie';//ie7
            }else if (ver.indexOf("msie 8.") != -1){    name = 'ie';//ie8
            }else if (ver.indexOf("msie 9.") != -1){    name = 'ie';//ie9
            }else if (ver.indexOf("msie 10.") != -1){   name = 'ie';//ie10
            }else{                                      name = 'ie';}
        }else if(ua.indexOf('trident/7') != -1){        name = 'ie';//ie11
    	}else if (ua.indexOf('edge') != -1){         	name = 'ie';//ie edge
        }else if (ua.indexOf('chrome') != -1){          name = 'chrome';
        }else if (ua.indexOf('safari') != -1){          name = 'safari';
        }else if (ua.indexOf('opera') != -1){           name = 'opera';
        }else if (ua.indexOf('firefox') != -1){         name = 'firefox';        }
        return name;
    };

    //==========================================================
    // ■IEのversion判定
    // http://developers.wano.co.jp/ie11-useragent-js%E3%81%AEie%E5%88%A4%E5%AE%9A%E5%BC%8F/
    //==========================================================
    BrowserUtil.getIEVersion = function(){
        var userAgent = window.navigator.userAgent.toLowerCase();
        var num;
        if( userAgent.match(/(msie|MSIE)/) || userAgent.match(/(T|t)rident/) ) {
            var ieVersion = userAgent.match(/((msie|MSIE)\s|rv:)([\d\.]+)/)[3];
            num = parseInt(ieVersion);
        } else {
            num = -1;
        }
        return num;
    };

    //=============================================================
    // ■スクロールバーを含まないウインドウサイズを取得 (object型 / width:幅, height:高さ )
    //=============================================================
    BrowserUtil.getWindowSize= function(){
        var obj = new Object();
        obj.width = $(window).width();
        obj.height = $(window).height();
        return obj;
    };

	//=============================================================
	// ■スクロールバーを含むウインドウサイズを取得 (object型 / width:幅, height:高さ )
	// http://black-flag.net/devel/windowWidthHeightCheck/
	//=============================================================
	BrowserUtil.getInnerWindowSize = function(){
		var obj = new Object();
		obj.width = window.innerWidth;
		obj.height = window.innerHeight;
		return obj;
	};

    //=============================================================
    // ■ドキュメントサイズ（ページサイズ）を取得 (object型 / width:幅, height:高さ )
    //=============================================================
    BrowserUtil.getDocumentSize= function(){
        var obj = new Object();
        obj.width = document.documentElement.scrollWidth || document.body.scrollWidth;
        obj.height = document.documentElement.scrollHeight || document.body.scrollHeight;
        return obj;
    };

	//=============================================================
	// ■横スクロールバーがあるかどうかの判別:Boolean
	//=============================================================
	BrowserUtil.hasScrollBar = function(){
		return ( $(window).width() == window.innerWidth ) ? false : true;
	};

	//=============================================================
    // ■スクロール位置を取得 (object型 / top:縦値, left:横値 )
    //=============================================================
    BrowserUtil.getScrollPosition = function() {
        var obj = new Object();
        obj.top = document.documentElement.scrollTop  || document.body.scrollTop;
        obj.left = document.documentElement.scrollLeft || document.body.scrollLeft;
        return obj;
    };

    //==========================================================
    // ■下からのスクロール位置を取得する
    // http://qiita.com/hoto17296/items/be4c1362647dd241905d
    //==========================================================
    BrowserUtil.getScrollBottom = function(){
        var body = window.document.body;
        var html = window.document.documentElement;
        var scrollTop = body.scrollTop || html.scrollTop;
        return html.scrollHeight - html.clientHeight - scrollTop;
    };


	return BrowserUtil;
})();



/****************************************************************
 *
 *  DeviceUtil [update:2018/1/14]
 *
 ****************************************************************/
var DeviceUtil = (function() {
    function DeviceUtil() {}

    //============================================================
    //  [powered by device.js]
    //  https://github.com/matthewhudson/current-device
    //============================================================

    // ■モバイル端末(mobile or tablet)か否かを返す
    //--------------------------------------------------------------
    DeviceUtil.isMobile = function(){
        var flag = false;
        if( device.default.mobile() || device.default.tablet() )  flag = true;
        return flag;
    };

    // ■デバイス名を返す
    // return: 'mobile', 'tablet', 'desktop', 'unknown'
    //--------------------------------------------------------------
    DeviceUtil.getType = function(){	return device.default.type;   };

    // ■OS名を返す
    // return: 'ios','android','blackberry','windows','fxos','meego','television', or 'unknown'
    //--------------------------------------------------------------
    DeviceUtil.getOs = function(){		return device.default.os;    };

    // ■向きを返す
    // return: "landscape(横)", "portrait(縦)", "unknown（不明：PC等）"
    //--------------------------------------------------------------
    DeviceUtil.getDirection = function(){return device.default.orientation;    };


    return DeviceUtil;
})();



/****************************************************************
 *
 *  SNSUtil [update:2017/06/06]
 *
 ****************************************************************/
var SNSUtil = (function() {
    function SNSUtil() {}

    //============================================================
    // Facebook シェアボタンのリンクを返す
    //============================================================
    SNSUtil.getFBLink = function(shareUrl){
        var base = 'https://www.facebook.com/share.php';
        var link = base + "?u=" + shareUrl;
        return link;
    };

    //============================================================
    // Twitter シェアボタンのリンクを返す
    //============================================================
    SNSUtil.getTWLink = function(text, shareUrl){
        var base = 'https://twitter.com/intent/tweet';
        var txt = encodeURI(text);
        var link = base + "?text=" + txt + "&url=" + shareUrl;
        return link;
    };

    //============================================================
    // LINE シェアボタンのリンクを返す
    //============================================================
    SNSUtil.getLineLink = function(text, shareUrl){
        var base = 'https://line.me/R/msg/text/';
        var txt = encodeURI(text);
        var link = base + "?" + txt + "%0D%0A" + shareUrl;
        return link;
    };

    //============================================================
    // Google+ シェアボタンのリンクを返す
    //============================================================
    SNSUtil.getGplusLink = function(shareUrl){
        var base = 'https://plus.google.com/share?url=';
        var link = base + shareUrl;
        return link;
    };

	//============================================================
	// メーラーを立ち上げる
	// http://rukiadia.hatenablog.jp/entry/2014/02/19/132445
	//============================================================
	SNSUtil.openMailer = function(to, subject, bodyText){
		location.href = 'mailto:' + to + '?subject=' + subject + '&body=' + bodyText;
	};


    return SNSUtil;
})();



/****************************************************************
 *
 *  ImgUtil [update:2016/10/28]
 *
 ****************************************************************/
var ImgUtil = (function() {
    function ImgUtil() {}

    //==========================================================
    // ■接尾辞の付いたファイル名を返す
    //==========================================================
    ImgUtil.getImgSrc = function( imgDefSrc, suffix ){
        var _temp = imgDefSrc.split("/");
        var _numSlashes = _temp.length;
        var baseSrc = "";
        for(var i=0; i <_temp.length-1; i++ ){  baseSrc += (_temp[i]+"/");  }
        var extension = _temp[_numSlashes-1].match(/.gif$|.jpg$|.png$/);
        var _temp2 = extension.input.split(".");
        var fileName = _temp2[0] + suffix + "." + _temp2[1];
        return baseSrc + fileName;
    };


    return ImgUtil;
})();



/****************************************************************
 *
 *  ColorUtil [update:2018/2/16]
 *
 ****************************************************************/
var ColorUtil = (function() {
	function ColorUtil() {}

	//==========================================================
	// ■ hslからrgbに変換して object型 で返す
	// http://blog.asial.co.jp/893
	//
	// hue 色相。0〜360の数値を指定
	// saturation 彩度 0〜100%の値を指定
	// lightness 明度 0〜100%の値を指定
	//==========================================================
	ColorUtil.hslToRgb = function(hue, saturation, lightness){
		var h = Number(hue),
			s = Number(saturation.replace('%', '')) / 100,
			l = Number(lightness.replace('%', '')) / 100,
			max = l <= 0.5 ? l * (1 + s) : l * (1 - s) + s,
			min = 2 * l - max,
			rgb = {};

		if (s == 0) {
			rgb.r = rgb.g = rgb.b = l;
		} else {
			var list = {};

			list['r'] = h >= 240 ? h - 240 : h + 120;
			list['g'] = h;
			list['b'] = h < 120 ? h + 240 : h - 120;

			for (var key in list) {
				var val = list[key],
					res;

				switch (true) {
					case val < 60:
						res = min + (max - min) * val / 60;
						break;
					case val < 180:
						res = max;
						break;
					case val < 240:
						res = min + (max - min) * (240 - val) / 60;
						break;
					case val < 360:
						res = min;
						break;
				}

				rgb[key] = res;
			}
		}

		// CSS用
		rgb.css = 'rgb(' + Math.round(rgb.r * 255) + ',' + Math.round(rgb.g * 255) + ',' + Math.round(rgb.b * 255) + ')';

		return rgb;
	};


	//==========================================================
	// ■ rgbからhslに変換して object型 で返す
	// http://blog.asial.co.jp/893
	//
	// red 赤。0〜255の数値を指定
	// green 緑。 0〜255の値を指定
	// blue 青。 0〜255の値を指定
	//==========================================================
	ColorUtil.rgbToHsl = function(red, green, blue){
		var r = red / 255,
			g = green / 255,
			b = blue / 255,
			rgb = {
				'r': r,
				'g': g,
				'b': b
			},
			max = Math.max(r, g, b),
			min = Math.min(r, g, b),
			hsl = {
				'h': 0,
				's': 0,
				'l': (max + min) / 2
			};

		// 彩度と色相の算出
		if (max != min) {
			// 彩度
			var m = hsl.l <= 0.5 ? (max + min) : (2 - max - min);
			hsl.s = (max - min) / m;

			// 色相
			var c = {};
			for (var k in rgb) {
				var v = rgb[k];
				c[k] = (max - v) / (max - min);
			}

			var h;
			switch (max) {
				case r:
					h = c.b - c.g;
					break;
				case g:
					h = 2 + c.r - c.b;
					break;
				case b:
					h = 4 + c.g - c.r;
					break;
			}

			h = h * 60;
			hsl.h = h < 0 ? h + 360 : h;
		}

		// CSS用
		hsl.css = 'hsl(' + hsl.h + ', ' + hsl.s * 100 + '%, ' + hsl.l * 100 + '%)';

		return hsl;
	};




	return ColorUtil;
})();
