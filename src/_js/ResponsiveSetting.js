/*********************************************************
 *
 *  ResponsiveSetting [update:2017/06/15]
 *
 *  レスポンシブ設定はここで。
 *  window が Resize される度に updateViewMode() しよう。
 *  call() で通知を呼び出せる。
 *  難点はページ読み込み直後0秒で反映できないこと。
 *
 *  [1]body#ID の切り替え(PC/SP)
 *  [2]viewportの切り替え
 *  [3]viewportの切り替えイベントを通知
 *  [4]img.resimg の画像切り替え
 *
 ********************************************************/
var ResponsiveSetting = (function() {
    function ResponsiveSetting() {

        var _this    = this;

        // property
        var _preMode = "";
        var _curMode = "";

		// getter
        //--------------------------------------------------
        this.getViewMode = function(){ return _curMode  };


        //=========================================================
        // ■コンストラクタ
        //=========================================================
        this.constructor = function(){
            $(window).on('resize load', _onResize);
            _onResize();
        };
        var _onResize = function(){
            _updateViewMode();
        }


        //------------------------------------------------------
        // ■ 表示更新
        //------------------------------------------------------
        var _updateViewMode = function(){

            _preMode = _curMode;

            // [1]ID切り替え
            if($('#breakpointChecker .pc').is(':visible')) {
                $("body").attr("id", "PC");
                Data.viewMode = _curMode = "PC";
            }else{
                $("body").attr("id", "SP");
                Data.viewMode = _curMode = "SP";
            }

            if( _preMode != _curMode ){

                // [2]viewport切り替え
                /*//
                switch(_curMode){
                    case "PC":  $("meta[name='viewport']").attr({ content:"width=1000,user-scalable=yes" });        break;
                    case "SP":  $("meta[name='viewport']").attr({ content:"width=device-width,user-scalable=no" }); break;
                }
                //*/

                // [3]イベントを通知
                emitter.emit(EVENT.SWITCH);
                switch(_curMode){
                    case "PC"   :  emitter.emit(EVENT.SWITCH_PC);  break;
                    case "SP"   :  emitter.emit(EVENT.SWITCH_SP);  break;
                }

                // [4]モードによって画像差し替え
				// data-src に指定したパスを書き換えてsrcへ (hoge.jpg → pc_hoge.jpg,sp_hoge.jpg)
				/*//
                if( _curMode == "PC" ){
                    $('.resimg').each(function(){
                        $(this).attr("src", "pc_"+$(this).data("src"));
                    });
                }else if( _curMode == "SP" ){
                    $('.resimg').each(function(){
                        $(this).attr("src", "sp_"+$(this).data("src"));
                    });
                }
                //*/
	        }
        }

		//------------------------------------------------------
        // ■ 通知を呼ぶ
        //------------------------------------------------------
        this.call = function(){
	        emitter.emit(EVENT.SWITCH);
            switch(_curMode){
                case "PC"   :  emitter.emit(EVENT.SWITCH_PC);  break;
                case "SP"   :  emitter.emit(EVENT.SWITCH_SP);  break;
            }
        }



        this.constructor();
    }


    return ResponsiveSetting;
})();
