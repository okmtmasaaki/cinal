/**********************************************************
 *  Data
 **********************************************************/
var Data = (function() {
	Data.viewMode;	//"PC","SP"

	function Data(){}
	return Data;
})();
/**********************************************************
 *  EVENT
 **********************************************************/
var EVENT = (function() {
	EVENT.SWITCH	= "switch";
	EVENT.SWITCH_PC = "switch.pc";
	EVENT.SWITCH_SP = "switch.sp";

	function EVENT(){}
	return EVENT;
})();


var emitter;
var responsive;

/****************************************************************
 *  Common
 ****************************************************************/
jQuery(function($){

	// EventEmitter2
	emitter = new EventEmitter2();

	// ResponsiveSetting
	responsive = new ResponsiveSetting();

	// RollOverButton
	$('.mouseover').rollOver({
		overImg:"_o"
	});
	$('.mouseover2').rollOver({
		mode:"alpha",
		overAlpha:"0.75"
	});

	// SmoothScroll

	switch(Data.viewMode){
	case "PC":
		$('a[href*="#"]').pageScroll({offsetY:0});
	break;
	case "SP":
		$('a[href*="#"]').pageScroll({offsetY:80});
	break;
	}

	//$('a[href*="#"]').pageScroll();

	// viewMode切替時に強制リロード
	emitter.on(EVENT.SWITCH, function(){
		location.reload();
	});

});
