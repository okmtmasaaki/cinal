

// plugins [update:2018/1/14]



/*!
 * current-device v0.7.2 - https://github.com/matthewhudson/current-device
 * MIT Licensed
 */
!function(n,o){"object"==typeof exports&&"object"==typeof module?module.exports=o():"function"==typeof define&&define.amd?define([],o):"object"==typeof exports?exports.device=o():n.device=o()}(this,function(){return function(n){function o(t){if(e[t])return e[t].exports;var i=e[t]={i:t,l:!1,exports:{}};return n[t].call(i.exports,i,i.exports,o),i.l=!0,i.exports}var e={};return o.m=n,o.c=e,o.d=function(n,e,t){o.o(n,e)||Object.defineProperty(n,e,{configurable:!1,enumerable:!0,get:t})},o.n=function(n){var e=n&&n.__esModule?function(){return n.default}:function(){return n};return o.d(e,"a",e),e},o.o=function(n,o){return Object.prototype.hasOwnProperty.call(n,o)},o.p="",o(o.s=0)}([function(n,o,e){n.exports=e(1)},function(n,o,e){"use strict";function t(n){return-1!==m.indexOf(n)}function i(n){return w.className.match(new RegExp(n,"i"))}function r(n){var o=null;i(n)||(o=w.className.replace(/^\s+|\s+$/g,""),w.className=o+" "+n)}function a(n){i(n)&&(w.className=w.className.replace(" "+n,""))}function d(){b.landscape()?(a("portrait"),r("landscape"),c("landscape")):(a("landscape"),r("portrait"),c("portrait")),l()}function c(n){for(var o in p)p[o](n)}function u(n){for(var o=0;o<n.length;o++)if(b[n[o]]())return n[o];return"unknown"}function l(){b.orientation=u(["portrait","landscape"])}Object.defineProperty(o,"__esModule",{value:!0});var s="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(n){return typeof n}:function(n){return n&&"function"==typeof Symbol&&n.constructor===Symbol&&n!==Symbol.prototype?"symbol":typeof n},f=window.device,b={},p=[];window.device=b;var w=window.document.documentElement,m=window.navigator.userAgent.toLowerCase(),v=["googletv","viera","smarttv","internet.tv","netcast","nettv","appletv","boxee","kylo","roku","dlnadoc","roku","pov_tv","hbbtv","ce-html"];b.macos=function(){return t("mac")},b.ios=function(){return b.iphone()||b.ipod()||b.ipad()},b.iphone=function(){return!b.windows()&&t("iphone")},b.ipod=function(){return t("ipod")},b.ipad=function(){return t("ipad")},b.android=function(){return!b.windows()&&t("android")},b.androidPhone=function(){return b.android()&&t("mobile")},b.androidTablet=function(){return b.android()&&!t("mobile")},b.blackberry=function(){return t("blackberry")||t("bb10")||t("rim")},b.blackberryPhone=function(){return b.blackberry()&&!t("tablet")},b.blackberryTablet=function(){return b.blackberry()&&t("tablet")},b.windows=function(){return t("windows")},b.windowsPhone=function(){return b.windows()&&t("phone")},b.windowsTablet=function(){return b.windows()&&t("touch")&&!b.windowsPhone()},b.fxos=function(){return(t("(mobile")||t("(tablet"))&&t(" rv:")},b.fxosPhone=function(){return b.fxos()&&t("mobile")},b.fxosTablet=function(){return b.fxos()&&t("tablet")},b.meego=function(){return t("meego")},b.cordova=function(){return window.cordova&&"file:"===location.protocol},b.nodeWebkit=function(){return"object"===s(window.process)},b.mobile=function(){return b.androidPhone()||b.iphone()||b.ipod()||b.windowsPhone()||b.blackberryPhone()||b.fxosPhone()||b.meego()},b.tablet=function(){return b.ipad()||b.androidTablet()||b.blackberryTablet()||b.windowsTablet()||b.fxosTablet()},b.desktop=function(){return!b.tablet()&&!b.mobile()},b.television=function(){for(var n=0;n<v.length;){if(t(v[n]))return!0;n++}return!1},b.portrait=function(){return window.innerHeight/window.innerWidth>1},b.landscape=function(){return window.innerHeight/window.innerWidth<1},b.noConflict=function(){return window.device=f,this},b.ios()?b.ipad()?r("ios ipad tablet"):b.iphone()?r("ios iphone mobile"):b.ipod()&&r("ios ipod mobile"):b.macos()?r("macos desktop"):b.android()?r(b.androidTablet()?"android tablet":"android mobile"):b.blackberry()?r(b.blackberryTablet()?"blackberry tablet":"blackberry mobile"):b.windows()?r(b.windowsTablet()?"windows tablet":b.windowsPhone()?"windows mobile":"windows desktop"):b.fxos()?r(b.fxosTablet()?"fxos tablet":"fxos mobile"):b.meego()?r("meego mobile"):b.nodeWebkit()?r("node-webkit"):b.television()?r("television"):b.desktop()&&r("desktop"),b.cordova()&&r("cordova"),b.onChangeOrientation=function(n){"function"==typeof n&&p.push(n)};var y="resize";Object.prototype.hasOwnProperty.call(window,"onorientationchange")&&(y="onorientationchange"),window.addEventListener?window.addEventListener(y,d,!1):window.attachEvent?window.attachEvent(y,d):window[y]=d,d(),b.type=u(["mobile","tablet","desktop"]),b.os=u(["ios","iphone","ipad","ipod","android","blackberry","windows","fxos","meego","television"]),l(),o.default=b}])});
//# sourceMappingURL=current-device.min.js.map




/*!
 * EventEmitter2
 * https://github.com/hij1nx/EventEmitter2
 *
 * Copyright (c) 2013 hij1nx
 * Licensed under the MIT license.
 */
!function(e){function r(){this._events={};if(this._conf){i.call(this,this._conf)}}function i(e){if(e){this._conf=e;e.delimiter&&(this.delimiter=e.delimiter);e.maxListeners&&(this._events.maxListeners=e.maxListeners);e.wildcard&&(this.wildcard=e.wildcard);e.newListener&&(this.newListener=e.newListener);if(this.wildcard){this.listenerTree={}}}}function s(e){this._events={};this.newListener=false;i.call(this,e)}function o(e,t,n,r){if(!n){return[]}var i=[],s,u,a,f,l,c,h,p=t.length,d=t[r],v=t[r+1];if(r===p&&n._listeners){if(typeof n._listeners==="function"){e&&e.push(n._listeners);return[n]}else{for(s=0,u=n._listeners.length;s<u;s++){e&&e.push(n._listeners[s])}return[n]}}if(d==="*"||d==="**"||n[d]){if(d==="*"){for(a in n){if(a!=="_listeners"&&n.hasOwnProperty(a)){i=i.concat(o(e,t,n[a],r+1))}}return i}else if(d==="**"){h=r+1===p||r+2===p&&v==="*";if(h&&n._listeners){i=i.concat(o(e,t,n,p))}for(a in n){if(a!=="_listeners"&&n.hasOwnProperty(a)){if(a==="*"||a==="**"){if(n[a]._listeners&&!h){i=i.concat(o(e,t,n[a],p))}i=i.concat(o(e,t,n[a],r))}else if(a===v){i=i.concat(o(e,t,n[a],r+2))}else{i=i.concat(o(e,t,n[a],r))}}}return i}i=i.concat(o(e,t,n[d],r+1))}f=n["*"];if(f){o(e,t,f,r+1)}l=n["**"];if(l){if(r<p){if(l._listeners){o(e,t,l,p)}for(a in l){if(a!=="_listeners"&&l.hasOwnProperty(a)){if(a===v){o(e,t,l[a],r+2)}else if(a===d){o(e,t,l[a],r+1)}else{c={};c[a]=l[a];o(e,t,{"**":c},r+1)}}}}else if(l._listeners){o(e,t,l,p)}else if(l["*"]&&l["*"]._listeners){o(e,t,l["*"],p)}}return i}function u(e,r){e=typeof e==="string"?e.split(this.delimiter):e.slice();for(var i=0,s=e.length;i+1<s;i++){if(e[i]==="**"&&e[i+1]==="**"){return}}var o=this.listenerTree;var u=e.shift();while(u){if(!o[u]){o[u]={}}o=o[u];if(e.length===0){if(!o._listeners){o._listeners=r}else if(typeof o._listeners==="function"){o._listeners=[o._listeners,r]}else if(t(o._listeners)){o._listeners.push(r);if(!o._listeners.warned){var a=n;if(typeof this._events.maxListeners!=="undefined"){a=this._events.maxListeners}if(a>0&&o._listeners.length>a){o._listeners.warned=true;console.error("(node) warning: possible EventEmitter memory "+"leak detected. %d listeners added. "+"Use emitter.setMaxListeners() to increase limit.",o._listeners.length);console.trace()}}}return true}u=e.shift()}return true}var t=Array.isArray?Array.isArray:function(t){return Object.prototype.toString.call(t)==="[object Array]"};var n=10;s.prototype.delimiter=".";s.prototype.setMaxListeners=function(e){this._events||r.call(this);this._events.maxListeners=e;if(!this._conf)this._conf={};this._conf.maxListeners=e};s.prototype.event="";s.prototype.once=function(e,t){this.many(e,1,t);return this};s.prototype.many=function(e,t,n){function i(){if(--t===0){r.off(e,i)}n.apply(this,arguments)}var r=this;if(typeof n!=="function"){throw new Error("many only accepts instances of Function")}i._origin=n;this.on(e,i);return r};s.prototype.emit=function(){this._events||r.call(this);var e=arguments[0];if(e==="newListener"&&!this.newListener){if(!this._events.newListener){return false}}if(this._all){var t=arguments.length;var n=new Array(t-1);for(var i=1;i<t;i++)n[i-1]=arguments[i];for(i=0,t=this._all.length;i<t;i++){this.event=e;this._all[i].apply(this,n)}}if(e==="error"){if(!this._all&&!this._events.error&&!(this.wildcard&&this.listenerTree.error)){if(arguments[1]instanceof Error){throw arguments[1]}else{throw new Error("Uncaught, unspecified 'error' event.")}return false}}var s;if(this.wildcard){s=[];var u=typeof e==="string"?e.split(this.delimiter):e.slice();o.call(this,s,u,this.listenerTree,0)}else{s=this._events[e]}if(typeof s==="function"){this.event=e;if(arguments.length===1){s.call(this)}else if(arguments.length>1)switch(arguments.length){case 2:s.call(this,arguments[1]);break;case 3:s.call(this,arguments[1],arguments[2]);break;default:var t=arguments.length;var n=new Array(t-1);for(var i=1;i<t;i++)n[i-1]=arguments[i];s.apply(this,n)}return true}else if(s){var t=arguments.length;var n=new Array(t-1);for(var i=1;i<t;i++)n[i-1]=arguments[i];var a=s.slice();for(var i=0,t=a.length;i<t;i++){this.event=e;a[i].apply(this,n)}return a.length>0||!!this._all}else{return!!this._all}};s.prototype.on=function(e,i){if(typeof e==="function"){this.onAny(e);return this}if(typeof i!=="function"){throw new Error("on only accepts instances of Function")}this._events||r.call(this);this.emit("newListener",e,i);if(this.wildcard){u.call(this,e,i);return this}if(!this._events[e]){this._events[e]=i}else if(typeof this._events[e]==="function"){this._events[e]=[this._events[e],i]}else if(t(this._events[e])){this._events[e].push(i);if(!this._events[e].warned){var s=n;if(typeof this._events.maxListeners!=="undefined"){s=this._events.maxListeners}if(s>0&&this._events[e].length>s){this._events[e].warned=true;console.error("(node) warning: possible EventEmitter memory "+"leak detected. %d listeners added. "+"Use emitter.setMaxListeners() to increase limit.",this._events[e].length);console.trace()}}}return this};s.prototype.onAny=function(e){if(typeof e!=="function"){throw new Error("onAny only accepts instances of Function")}if(!this._all){this._all=[]}this._all.push(e);return this};s.prototype.addListener=s.prototype.on;s.prototype.off=function(e,n){if(typeof n!=="function"){throw new Error("removeListener only takes instances of Function")}var r,i=[];if(this.wildcard){var s=typeof e==="string"?e.split(this.delimiter):e.slice();i=o.call(this,null,s,this.listenerTree,0)}else{if(!this._events[e])return this;r=this._events[e];i.push({_listeners:r})}for(var u=0;u<i.length;u++){var a=i[u];r=a._listeners;if(t(r)){var f=-1;for(var l=0,c=r.length;l<c;l++){if(r[l]===n||r[l].listener&&r[l].listener===n||r[l]._origin&&r[l]._origin===n){f=l;break}}if(f<0){continue}if(this.wildcard){a._listeners.splice(f,1)}else{this._events[e].splice(f,1)}if(r.length===0){if(this.wildcard){delete a._listeners}else{delete this._events[e]}}return this}else if(r===n||r.listener&&r.listener===n||r._origin&&r._origin===n){if(this.wildcard){delete a._listeners}else{delete this._events[e]}}}return this};s.prototype.offAny=function(e){var t=0,n=0,r;if(e&&this._all&&this._all.length>0){r=this._all;for(t=0,n=r.length;t<n;t++){if(e===r[t]){r.splice(t,1);return this}}}else{this._all=[]}return this};s.prototype.removeListener=s.prototype.off;s.prototype.removeAllListeners=function(e){if(arguments.length===0){!this._events||r.call(this);return this}if(this.wildcard){var t=typeof e==="string"?e.split(this.delimiter):e.slice();var n=o.call(this,null,t,this.listenerTree,0);for(var i=0;i<n.length;i++){var s=n[i];s._listeners=null}}else{if(!this._events[e])return this;this._events[e]=null}return this};s.prototype.listeners=function(e){if(this.wildcard){var n=[];var i=typeof e==="string"?e.split(this.delimiter):e.slice();o.call(this,n,i,this.listenerTree,0);return n}this._events||r.call(this);if(!this._events[e])this._events[e]=[];if(!t(this._events[e])){this._events[e]=[this._events[e]]}return this._events[e]};s.prototype.listenersAny=function(){if(this._all){return this._all}else{return[]}};if(typeof define==="function"&&define.amd){define(function(){return s})}else if(typeof exports==="object"){exports.EventEmitter2=s}else{window.EventEmitter2=s}}()



/* Promiseのpolyfill (for IE11)
 * http://qiita.com/zERobYTezERo/items/ac458c4cf2a8f2f6c3dd
 */
!function n(t,e,o){function i(u,f){if(!e[u]){if(!t[u]){var c="function"==typeof require&&require;if(!f&&c)return c(u,!0);if(r)return r(u,!0);var s=new Error("Cannot find module '"+u+"'");throw s.code="MODULE_NOT_FOUND",s}var a=e[u]={exports:{}};t[u][0].call(a.exports,function(n){var e=t[u][1][n];return i(e?e:n)},a,a.exports,n,t,e,o)}return e[u].exports}for(var r="function"==typeof require&&require,u=0;u<o.length;u++)i(o[u]);return i}({1:[function(n,t){function e(){}var o=t.exports={};o.nextTick=function(){var n="undefined"!=typeof window&&window.setImmediate,t="undefined"!=typeof window&&window.postMessage&&window.addEventListener;if(n)return function(n){return window.setImmediate(n)};if(t){var e=[];return window.addEventListener("message",function(n){var t=n.source;if((t===window||null===t)&&"process-tick"===n.data&&(n.stopPropagation(),e.length>0)){var o=e.shift();o()}},!0),function(n){e.push(n),window.postMessage("process-tick","*")}}return function(n){setTimeout(n,0)}}(),o.title="browser",o.browser=!0,o.env={},o.argv=[],o.on=e,o.addListener=e,o.once=e,o.off=e,o.removeListener=e,o.removeAllListeners=e,o.emit=e,o.binding=function(){throw new Error("process.binding is not supported")},o.cwd=function(){return"/"},o.chdir=function(){throw new Error("process.chdir is not supported")}},{}],2:[function(n,t){"use strict";function e(n){function t(n){return null===c?void a.push(n):void r(function(){var t=c?n.onFulfilled:n.onRejected;if(null===t)return void(c?n.resolve:n.reject)(s);var e;try{e=t(s)}catch(o){return void n.reject(o)}n.resolve(e)})}function e(n){try{if(n===l)throw new TypeError("A promise cannot be resolved with itself.");if(n&&("object"==typeof n||"function"==typeof n)){var t=n.then;if("function"==typeof t)return void i(t.bind(n),e,u)}c=!0,s=n,f()}catch(o){u(o)}}function u(n){c=!1,s=n,f()}function f(){for(var n=0,e=a.length;e>n;n++)t(a[n]);a=null}if("object"!=typeof this)throw new TypeError("Promises must be constructed via new");if("function"!=typeof n)throw new TypeError("not a function");var c=null,s=null,a=[],l=this;this.then=function(n,e){return new l.constructor(function(i,r){t(new o(n,e,i,r))})},i(n,e,u)}function o(n,t,e,o){this.onFulfilled="function"==typeof n?n:null,this.onRejected="function"==typeof t?t:null,this.resolve=e,this.reject=o}function i(n,t,e){var o=!1;try{n(function(n){o||(o=!0,t(n))},function(n){o||(o=!0,e(n))})}catch(i){if(o)return;o=!0,e(i)}}var r=n("asap");t.exports=e},{asap:4}],3:[function(n,t){"use strict";function e(n){this.then=function(t){return"function"!=typeof t?this:new o(function(e,o){i(function(){try{e(t(n))}catch(i){o(i)}})})}}var o=n("./core.js"),i=n("asap");t.exports=o,e.prototype=o.prototype;var r=new e(!0),u=new e(!1),f=new e(null),c=new e(void 0),s=new e(0),a=new e("");o.resolve=function(n){if(n instanceof o)return n;if(null===n)return f;if(void 0===n)return c;if(n===!0)return r;if(n===!1)return u;if(0===n)return s;if(""===n)return a;if("object"==typeof n||"function"==typeof n)try{var t=n.then;if("function"==typeof t)return new o(t.bind(n))}catch(i){return new o(function(n,t){t(i)})}return new e(n)},o.all=function(n){var t=Array.prototype.slice.call(n);return new o(function(n,e){function o(r,u){try{if(u&&("object"==typeof u||"function"==typeof u)){var f=u.then;if("function"==typeof f)return void f.call(u,function(n){o(r,n)},e)}t[r]=u,0===--i&&n(t)}catch(c){e(c)}}if(0===t.length)return n([]);for(var i=t.length,r=0;r<t.length;r++)o(r,t[r])})},o.reject=function(n){return new o(function(t,e){e(n)})},o.race=function(n){return new o(function(t,e){n.forEach(function(n){o.resolve(n).then(t,e)})})},o.prototype["catch"]=function(n){return this.then(null,n)}},{"./core.js":2,asap:4}],4:[function(n,t){(function(n){function e(){for(;i.next;){i=i.next;var n=i.task;i.task=void 0;var t=i.domain;t&&(i.domain=void 0,t.enter());try{n()}catch(o){if(c)throw t&&t.exit(),setTimeout(e,0),t&&t.enter(),o;setTimeout(function(){throw o},0)}t&&t.exit()}u=!1}function o(t){r=r.next={task:t,domain:c&&n.domain,next:null},u||(u=!0,f())}var i={task:void 0,next:null},r=i,u=!1,f=void 0,c=!1;if("undefined"!=typeof n&&n.nextTick)c=!0,f=function(){n.nextTick(e)};else if("function"==typeof setImmediate)f="undefined"!=typeof window?setImmediate.bind(window,e):function(){setImmediate(e)};else if("undefined"!=typeof MessageChannel){var s=new MessageChannel;s.port1.onmessage=e,f=function(){s.port2.postMessage(0)}}else f=function(){setTimeout(e,0)};t.exports=o}).call(this,n("_process"))},{_process:1}],5:[function(){"function"!=typeof Promise.prototype.done&&(Promise.prototype.done=function(){var n=arguments.length?this.then.apply(this,arguments):this;n.then(null,function(n){setTimeout(function(){throw n},0)})})},{}],6:[function(n){n("asap");"undefined"==typeof Promise&&(Promise=n("./lib/core.js"),n("./lib/es6-extensions.js")),n("./polyfill-done.js")},{"./lib/core.js":2,"./lib/es6-extensions.js":3,"./polyfill-done.js":5,asap:4}]},{},[6]);
//# sourceMappingURL=/polyfills/promise-6.1.0.min.js.map



// https://github.com/bfred-it/object-fit-images
// http://masaroku.com/?p=1063
/*! npm.im/object-fit-images 3.2.3 */
var objectFitImages=function(){"use strict";function t(t,e){return"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='"+t+"' height='"+e+"'%3E%3C/svg%3E"}function e(t){if(t.srcset&&!m&&window.picturefill){var e=window.picturefill._;t[e.ns]&&t[e.ns].evaled||e.fillImg(t,{reselect:!0}),t[e.ns].curSrc||(t[e.ns].supported=!1,e.fillImg(t,{reselect:!0})),t.currentSrc=t[e.ns].curSrc||t.src}}function i(t){for(var e,i=getComputedStyle(t).fontFamily,r={};null!==(e=l.exec(i));)r[e[1]]=e[2];return r}function r(e,i,r){var n=t(i||1,r||0);p.call(e,"src")!==n&&b.call(e,"src",n)}function n(t,e){t.naturalWidth?e(t):setTimeout(n,100,t,e)}function c(t){var c=i(t),o=t[a];if(c["object-fit"]=c["object-fit"]||"fill",!o.img){if("fill"===c["object-fit"])return;if(!o.skipTest&&g&&!c["object-position"])return}if(!o.img){o.img=new Image(t.width,t.height),o.img.srcset=p.call(t,"data-ofi-srcset")||t.srcset,o.img.src=p.call(t,"data-ofi-src")||t.src,b.call(t,"data-ofi-src",t.src),t.srcset&&b.call(t,"data-ofi-srcset",t.srcset),r(t,t.naturalWidth||t.width,t.naturalHeight||t.height),t.srcset&&(t.srcset="");try{s(t)}catch(t){window.console&&console.warn("https://bit.ly/ofi-old-browser")}}e(o.img),t.style.backgroundImage='url("'+(o.img.currentSrc||o.img.src).replace(/"/g,'\\"')+'")',t.style.backgroundPosition=c["object-position"]||"center",t.style.backgroundRepeat="no-repeat",t.style.backgroundOrigin="content-box",/scale-down/.test(c["object-fit"])?n(o.img,function(){o.img.naturalWidth>t.width||o.img.naturalHeight>t.height?t.style.backgroundSize="contain":t.style.backgroundSize="auto"}):t.style.backgroundSize=c["object-fit"].replace("none","auto").replace("fill","100% 100%"),n(o.img,function(e){r(t,e.naturalWidth,e.naturalHeight)})}function s(t){var e={get:function(e){return t[a].img[e||"src"]},set:function(e,i){return t[a].img[i||"src"]=e,b.call(t,"data-ofi-"+i,e),c(t),e}};Object.defineProperty(t,"src",e),Object.defineProperty(t,"currentSrc",{get:function(){return e.get("currentSrc")}}),Object.defineProperty(t,"srcset",{get:function(){return e.get("srcset")},set:function(t){return e.set(t,"srcset")}})}function o(t,e){var i=!h&&!t;if(e=e||{},t=t||"img",f&&!e.skipTest||!d)return!1;"img"===t?t=document.getElementsByTagName("img"):"string"==typeof t?t=document.querySelectorAll(t):"length"in t||(t=[t]);for(var r=0;r<t.length;r++)t[r][a]=t[r][a]||{skipTest:e.skipTest},c(t[r]);i&&(document.body.addEventListener("load",function(t){"IMG"===t.target.tagName&&o(t.target,{skipTest:e.skipTest})},!0),h=!0,t="img"),e.watchMQ&&window.addEventListener("resize",o.bind(null,t,{skipTest:e.skipTest}))}var a="bfred-it:object-fit-images",l=/(object-fit|object-position)\s*:\s*([-\w\s%]+)/g,u="undefined"==typeof Image?{style:{"object-position":1}}:new Image,g="object-fit"in u.style,f="object-position"in u.style,d="background-size"in u.style,m="string"==typeof u.currentSrc,p=u.getAttribute,b=u.setAttribute,h=!1;return o.supportsObjectFit=g,o.supportsObjectPosition=f,function(){function t(t,e){return t[a]&&t[a].img&&("src"===e||"srcset"===e)?t[a].img:t}f||(HTMLImageElement.prototype.getAttribute=function(e){return p.call(t(this,e),e)},HTMLImageElement.prototype.setAttribute=function(e,i){return b.call(t(this,e),e,String(i))})}(),o}();



//=========================================================================
//	▼ RollOver Button ▼
// default : ファイル名+"_over" の画像と切り替え
// alpha : 透明度を変更
//=========================================================================
var _rollOverFunc = function(){
    $.fn.rollOver = function(options) {
        if(this.length == 0) return;
        var c = $.extend({
            mode: "default",  //default, alpha, fade, blink
            overImg:"_over",
            outImg:"",
            currentImg:"",
            speed: 100,
            overAlpha:"0.75",
            outAlpha:"1"
        }, options || {});

        $(this).each(function() {
            var img = $(this);
            if(img.data("_isSet")) return;
            img.data("_isSet",true);

            if(c.mode == "fade"){
                var imgW = img.width();
                var imgH = img.height();
                var extension = img.attr("src").match(/.gif$|.jpg$|.png$/);
                var re = new RegExp(c.outImg+extension);
                var imgOverPath = img.attr("src").toString().replace(re,c.overImg+extension);
                $(img.parent()[0]).css({
                    width:imgW+"px",
                    height:imgH+"px",
                    background:"url("+imgOverPath+")",
                    display:"block"
                });
            }
            img.data("_options", c);
            img.hover(
                function(){
                    var c = img.data("_options");
                    if(c.mode == "default"){
                        var extension = img.attr("src").match(/.gif$|.jpg$|.png$/);
                        var re = new RegExp(c.outImg+extension);
                        img.attr("src",
                            img.attr("src").replace(re,c.overImg+extension)
                        );
                    } else if(c.mode == "alpha" || c.mode == "fade"){
                        img.stop().fadeTo(c.speed,c.overAlpha);
                    } else if(c.mode == "blink"){
                        img.stop().fadeTo(0,c.overAlpha).animate({ opacity: c.outAlpha}, c.speed , "easeInQuad" );
                    }
                },
                function(){
                    var c = img.data("_options");
                    if(c.mode == "default"){
                        var extension = img.attr("src").match(/.gif$|.jpg$|.png$/);
                        var re = new RegExp(c.overImg+extension);
                        img.attr("src",
                            img.attr("src").replace(re,c.outImg+extension)
                        );
                    } else if(c.mode == "alpha" || c.mode == "fade"){
                        img.stop().fadeTo(c.speed,c.outAlpha);
                    }
                }
            );
        });
    }
};
_rollOverFunc();



/****************************************************************
 *  SmoothScroll
 ****************************************************************/
var _smoothScrollFunc = function(){
    $.fn.pageScroll = function(options) {
        if(this.length == 0) return;
        var c = $.extend({
            offsetY:0,
            ignore:"ignore"
        }, options || {});

        this.click(function() {
            if($(this).hasClass(c.ignore)) return true;
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length && target;
                if (target.length) {
                    var sclpos = 0;
                    var scldurat = 1000;
                    var targetOffset = target.offset().top - sclpos;
                    var offsetY = c.offsetY;
                    $('html,body').stop()
                        .animate({scrollTop: targetOffset-offsetY}, {duration: scldurat,easing:"easeOutExpo"});
                    return false;
                }
            }
        });
    }
};
_smoothScrollFunc();



/*!=============================================================
 * Copyright (c) 2013 Brandon Aaron (http://brandon.aaron.sh)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 3.1.9
 *
 * Requires: jQuery 1.2.2+
 *=============================================================
 */
(function (factory) {
    if ( typeof define === 'function' && define.amd ) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS style for Browserify
        module.exports = factory;
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var toFix  = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'],
        toBind = ( 'onwheel' in document || document.documentMode >= 9 ) ?
                    ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'],
        slice  = Array.prototype.slice,
        nullLowestDeltaTimeout, lowestDelta;

    if ( $.event.fixHooks ) {
        for ( var i = toFix.length; i; ) {
            $.event.fixHooks[ toFix[--i] ] = $.event.mouseHooks;
        }
    }

    var special = $.event.special.mousewheel = {
        version: '3.1.9',

        setup: function() {
            if ( this.addEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.addEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = handler;
            }
            // Store the line height and page height for this particular element
            $.data(this, 'mousewheel-line-height', special.getLineHeight(this));
            $.data(this, 'mousewheel-page-height', special.getPageHeight(this));
        },

        teardown: function() {
            if ( this.removeEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.removeEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = null;
            }
        },

        getLineHeight: function(elem) {
            return parseInt($(elem)['offsetParent' in $.fn ? 'offsetParent' : 'parent']().css('fontSize'), 10);
        },

        getPageHeight: function(elem) {
            return $(elem).height();
        },

        settings: {
            adjustOldDeltas: true
        }
    };

    $.fn.extend({
        mousewheel: function(fn) {
            return fn ? this.bind('mousewheel', fn) : this.trigger('mousewheel');
        },

        unmousewheel: function(fn) {
            return this.unbind('mousewheel', fn);
        }
    });


    function handler(event) {
        var orgEvent   = event || window.event,
            args       = slice.call(arguments, 1),
            delta      = 0,
            deltaX     = 0,
            deltaY     = 0,
            absDelta   = 0;
        event = $.event.fix(orgEvent);
        event.type = 'mousewheel';

        // Old school scrollwheel delta
        if ( 'detail'      in orgEvent ) { deltaY = orgEvent.detail * -1;      }
        if ( 'wheelDelta'  in orgEvent ) { deltaY = orgEvent.wheelDelta;       }
        if ( 'wheelDeltaY' in orgEvent ) { deltaY = orgEvent.wheelDeltaY;      }
        if ( 'wheelDeltaX' in orgEvent ) { deltaX = orgEvent.wheelDeltaX * -1; }

        // Firefox < 17 horizontal scrolling related to DOMMouseScroll event
        if ( 'axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS ) {
            deltaX = deltaY * -1;
            deltaY = 0;
        }

        // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy
        delta = deltaY === 0 ? deltaX : deltaY;

        // New school wheel delta (wheel event)
        if ( 'deltaY' in orgEvent ) {
            deltaY = orgEvent.deltaY * -1;
            delta  = deltaY;
        }
        if ( 'deltaX' in orgEvent ) {
            deltaX = orgEvent.deltaX;
            if ( deltaY === 0 ) { delta  = deltaX * -1; }
        }

        // No change actually happened, no reason to go any further
        if ( deltaY === 0 && deltaX === 0 ) { return; }

        // Need to convert lines and pages to pixels if we aren't already in pixels
        // There are three delta modes:
        //   * deltaMode 0 is by pixels, nothing to do
        //   * deltaMode 1 is by lines
        //   * deltaMode 2 is by pages
        if ( orgEvent.deltaMode === 1 ) {
            var lineHeight = $.data(this, 'mousewheel-line-height');
            delta  *= lineHeight;
            deltaY *= lineHeight;
            deltaX *= lineHeight;
        } else if ( orgEvent.deltaMode === 2 ) {
            var pageHeight = $.data(this, 'mousewheel-page-height');
            delta  *= pageHeight;
            deltaY *= pageHeight;
            deltaX *= pageHeight;
        }

        // Store lowest absolute delta to normalize the delta values
        absDelta = Math.max( Math.abs(deltaY), Math.abs(deltaX) );

        if ( !lowestDelta || absDelta < lowestDelta ) {
            lowestDelta = absDelta;

            // Adjust older deltas if necessary
            if ( shouldAdjustOldDeltas(orgEvent, absDelta) ) {
                lowestDelta /= 40;
            }
        }

        // Adjust older deltas if necessary
        if ( shouldAdjustOldDeltas(orgEvent, absDelta) ) {
            // Divide all the things by 40!
            delta  /= 40;
            deltaX /= 40;
            deltaY /= 40;
        }

        // Get a whole, normalized value for the deltas
        delta  = Math[ delta  >= 1 ? 'floor' : 'ceil' ](delta  / lowestDelta);
        deltaX = Math[ deltaX >= 1 ? 'floor' : 'ceil' ](deltaX / lowestDelta);
        deltaY = Math[ deltaY >= 1 ? 'floor' : 'ceil' ](deltaY / lowestDelta);

        // Add information to the event object
        event.deltaX = deltaX;
        event.deltaY = deltaY;
        event.deltaFactor = lowestDelta;
        // Go ahead and set deltaMode to 0 since we converted to pixels
        // Although this is a little odd since we overwrite the deltaX/Y
        // properties with normalized deltas.
        event.deltaMode = 0;

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        // Clearout lowestDelta after sometime to better
        // handle multiple device types that give different
        // a different lowestDelta
        // Ex: trackpad = 3 and mouse wheel = 120
        if (nullLowestDeltaTimeout) { clearTimeout(nullLowestDeltaTimeout); }
        nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

    function nullLowestDelta() {
        lowestDelta = null;
    }

    function shouldAdjustOldDeltas(orgEvent, absDelta) {
        // If this is an older event and the delta is divisable by 120,
        // then we are assuming that the browser is treating this as an
        // older mouse wheel event and that we should divide the deltas
        // by 40 to try and get a more usable deltaFactor.
        // Side note, this actually impacts the reported scroll distance
        // in older browsers and can cause scrolling to be slower than native.
        // Turn this off by setting $.event.special.mousewheel.settings.adjustOldDeltas to false.
        return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
    }

}));



/*
 * Ease+ for Velocity.js v1.2.2
 * v1.2
 *
 * Copyright (c) 2015 Yuichiroh Arai
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 *
 *
 *
 * All easing functions from TweenJS
 * http://createjs.com/
 *
 * Copyright (c) 2011-2015 gskinner.com, inc.
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 *
 */
!function(t){if(t&&t.Velocity&&t.Velocity.Easings){var n=t.Velocity.Easings;t.easeplus=n,n.getBackIn=function(t){return function(n){return n*n*((t+1)*n-t)}},n.getBackOut=function(t){return function(n){return--n*n*((t+1)*n+t)+1}},n.getBackInOut=function(t){return t*=1.525,function(n){return(n*=2)<1?.5*n*n*((t+1)*n-t):.5*((n-=2)*n*((t+1)*n+t)+2)}},n.easeInBack=n.getBackIn(1.7),n.easeOutBack=n.getBackOut(1.7),n.easeInOutBack=n.getBackInOut(1.7),n.addBackIn=function(t,e){n[t]=n.getBackIn(e)},n.addBackOut=function(t,e){n[t]=n.getBackOut(e)},n.addBackInOut=function(t,e){n[t]=n.getBackInOut(e)},n.getElasticIn=function(t,n){var e=2*Math.PI;return function(u){if(0==u||1==u)return u;var a=n/e*Math.asin(1/t);return-(t*Math.pow(2,10*(u-=1))*Math.sin((u-a)*e/n))}},n.getElasticOut=function(t,n){var e=2*Math.PI;return function(u){if(0==u||1==u)return u;var a=n/e*Math.asin(1/t);return t*Math.pow(2,-10*u)*Math.sin((u-a)*e/n)+1}},n.getElasticInOut=function(t,n){var e=2*Math.PI;return function(u){var a=n/e*Math.asin(1/t);return(u*=2)<1?-.5*t*Math.pow(2,10*(u-=1))*Math.sin((u-a)*e/n):t*Math.pow(2,-10*(u-=1))*Math.sin((u-a)*e/n)*.5+1}},n.easeInElastic=n.getElasticIn(1,.3),n.easeOutElastic=n.getElasticOut(1,.3),n.easeInOutElastic=n.getElasticInOut(1,.3*1.5),n.addElasticIn=function(t,e,u){n[t]=n.getElasticIn(e,u)},n.addElasticOut=function(t,e,u){n[t]=n.getElasticOut(e,u)},n.addElasticInOut=function(t,e,u){n[t]=n.getElasticInOut(e,u)},n.easeInBounce=function(t){return 1-n.bounceOut(1-t)},n.easeOutBounce=function(t){return 1/2.75>t?7.5625*t*t:2/2.75>t?7.5625*(t-=1.5/2.75)*t+.75:2.5/2.75>t?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375},n.easeInOutBounce=function(t){return.5>t?.5*n.easeInBounce(2*t):.5*n.easeOutBounce(2*t-1)+.5},n.addBlend=function(t,e,u,a){"linear"===a&&(a=null),n[t]=function(t){var c=n[e](t),i=n[u](t),s=t;return a&&(s=n[a](t)),c+(i-c)*s}},n.remove=function(t){delete n[t]},n.sineIn=n.easeInSine,n.sineOut=n.easeOutSine,n.sineInOut=n.easeInOutSine,n.quadIn=n.easeInQuad,n.quadOut=n.easeOutQuad,n.quadInOut=n.easeInOutQuad,n.cubicIn=n.easeInCubic,n.cubicOut=n.easeOutCubic,n.cubicInOut=n.easeInOutCubic,n.quartIn=n.easeInQuart,n.quartOut=n.easeOutQuart,n.quartInOut=n.easeInOutQuart,n.quintIn=n.easeInQuint,n.quintOut=n.easeOutQuint,n.quintInOut=n.easeInOutQuint,n.expoIn=n.easeInExpo,n.expoOut=n.easeOutExpo,n.expoInOut=n.easeInOutExpo,n.circIn=n.easeInCirc,n.circOut=n.easeOutCirc,n.circInOut=n.easeInOutCirc,n.backIn=n.easeInBack,n.backOut=n.easeOutBack,n.backInOut=n.easeInOutBack,n.elasticIn=n.easeInElastic,n.elasticOut=n.easeOutElastic,n.elasticInOut=n.easeInOutElastic,n.bounceIn=n.easeInBounce,n.bounceOut=n.easeOutBounce,n.bounceInOut=n.easeInOutBounce}}(window.jQuery);



/*!
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 *
 * Open source under the BSD License.
 *
 * Copyright ﾂｩ 2008 George McGinley Smith
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 *
 * Neither the name of the author nor the names of contributors may be used to endorse
 * or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

// t: current time, b: begInnIng value, c: change In value, d: duration
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
	def: 'easeOutQuad',
	swing: function (x, t, b, c, d) {
		//alert(jQuery.easing.default);
		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c*(t/=d)*t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c *(t/=d)*(t-2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t + b;
		return -c/2 * ((--t)*(t-2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c*(t/=d)*t*t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t=t/d-1)*t*t*t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
		return -c/2 * ((t-=2)*t*t*t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c*(t/=d)*t*t*t*t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t*t*t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
		return c/2*((t-=2)*t*t*t*t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
		return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
		return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*(t/=d)*t*((s+1)*t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
		return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t/=d) < (1/2.75)) {
			return c*(7.5625*t*t) + b;
		} else if (t < (2/2.75)) {
			return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
		} else if (t < (2.5/2.75)) {
			return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
		} else {
			return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
	}
});



/**
 * A class to parse color values
 * @author Stoyan Stefanov <sstoo@gmail.com>
 * @link   http://www.phpied.com/rgb-color-parser-in-javascript/
 * @license MIT license
 */
/*
* (使い方)
* http://bit.ly/2laWSoj
 */
function RGBColor(e){this.ok=!1,"#"==e.charAt(0)&&(e=e.substr(1,6)),e=e.replace(/ /g,""),e=e.toLowerCase();var f={aliceblue:"f0f8ff",antiquewhite:"faebd7",aqua:"00ffff",aquamarine:"7fffd4",azure:"f0ffff",beige:"f5f5dc",bisque:"ffe4c4",black:"000000",blanchedalmond:"ffebcd",blue:"0000ff",blueviolet:"8a2be2",brown:"a52a2a",burlywood:"deb887",cadetblue:"5f9ea0",chartreuse:"7fff00",chocolate:"d2691e",coral:"ff7f50",cornflowerblue:"6495ed",cornsilk:"fff8dc",crimson:"dc143c",cyan:"00ffff",darkblue:"00008b",darkcyan:"008b8b",darkgoldenrod:"b8860b",darkgray:"a9a9a9",darkgreen:"006400",darkkhaki:"bdb76b",darkmagenta:"8b008b",darkolivegreen:"556b2f",darkorange:"ff8c00",darkorchid:"9932cc",darkred:"8b0000",darksalmon:"e9967a",darkseagreen:"8fbc8f",darkslateblue:"483d8b",darkslategray:"2f4f4f",darkturquoise:"00ced1",darkviolet:"9400d3",deeppink:"ff1493",deepskyblue:"00bfff",dimgray:"696969",dodgerblue:"1e90ff",feldspar:"d19275",firebrick:"b22222",floralwhite:"fffaf0",forestgreen:"228b22",fuchsia:"ff00ff",gainsboro:"dcdcdc",ghostwhite:"f8f8ff",gold:"ffd700",goldenrod:"daa520",gray:"808080",green:"008000",greenyellow:"adff2f",honeydew:"f0fff0",hotpink:"ff69b4",indianred:"cd5c5c",indigo:"4b0082",ivory:"fffff0",khaki:"f0e68c",lavender:"e6e6fa",lavenderblush:"fff0f5",lawngreen:"7cfc00",lemonchiffon:"fffacd",lightblue:"add8e6",lightcoral:"f08080",lightcyan:"e0ffff",lightgoldenrodyellow:"fafad2",lightgrey:"d3d3d3",lightgreen:"90ee90",lightpink:"ffb6c1",lightsalmon:"ffa07a",lightseagreen:"20b2aa",lightskyblue:"87cefa",lightslateblue:"8470ff",lightslategray:"778899",lightsteelblue:"b0c4de",lightyellow:"ffffe0",lime:"00ff00",limegreen:"32cd32",linen:"faf0e6",magenta:"ff00ff",maroon:"800000",mediumaquamarine:"66cdaa",mediumblue:"0000cd",mediumorchid:"ba55d3",mediumpurple:"9370d8",mediumseagreen:"3cb371",mediumslateblue:"7b68ee",mediumspringgreen:"00fa9a",mediumturquoise:"48d1cc",mediumvioletred:"c71585",midnightblue:"191970",mintcream:"f5fffa",mistyrose:"ffe4e1",moccasin:"ffe4b5",navajowhite:"ffdead",navy:"000080",oldlace:"fdf5e6",olive:"808000",olivedrab:"6b8e23",orange:"ffa500",orangered:"ff4500",orchid:"da70d6",palegoldenrod:"eee8aa",palegreen:"98fb98",paleturquoise:"afeeee",palevioletred:"d87093",papayawhip:"ffefd5",peachpuff:"ffdab9",peru:"cd853f",pink:"ffc0cb",plum:"dda0dd",powderblue:"b0e0e6",purple:"800080",red:"ff0000",rosybrown:"bc8f8f",royalblue:"4169e1",saddlebrown:"8b4513",salmon:"fa8072",sandybrown:"f4a460",seagreen:"2e8b57",seashell:"fff5ee",sienna:"a0522d",silver:"c0c0c0",skyblue:"87ceeb",slateblue:"6a5acd",slategray:"708090",snow:"fffafa",springgreen:"00ff7f",steelblue:"4682b4",tan:"d2b48c",teal:"008080",thistle:"d8bfd8",tomato:"ff6347",turquoise:"40e0d0",violet:"ee82ee",violetred:"d02090",wheat:"f5deb3",white:"ffffff",whitesmoke:"f5f5f5",yellow:"ffff00",yellowgreen:"9acd32"};for(var a in f)e==a&&(e=f[a]);for(var r=[{re:/^rgb\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)$/,example:["rgb(123, 234, 45)","rgb(255,234,245)"],process:function(e){return[parseInt(e[1]),parseInt(e[2]),parseInt(e[3])]}},{re:/^(\w{2})(\w{2})(\w{2})$/,example:["#00ff00","336699"],process:function(e){return[parseInt(e[1],16),parseInt(e[2],16),parseInt(e[3],16)]}},{re:/^(\w{1})(\w{1})(\w{1})$/,example:["#fb0","f0f"],process:function(e){return[parseInt(e[1]+e[1],16),parseInt(e[2]+e[2],16),parseInt(e[3]+e[3],16)]}}],t=0;t<r.length;t++){var d=r[t].re,l=r[t].process,n=d.exec(e);n&&(channels=l(n),this.r=channels[0],this.g=channels[1],this.b=channels[2],this.ok=!0)}this.r=this.r<0||isNaN(this.r)?0:this.r>255?255:this.r,this.g=this.g<0||isNaN(this.g)?0:this.g>255?255:this.g,this.b=this.b<0||isNaN(this.b)?0:this.b>255?255:this.b,this.toRGB=function(){return"rgb("+this.r+", "+this.g+", "+this.b+")"},this.toHex=function(){var e=this.r.toString(16),f=this.g.toString(16),a=this.b.toString(16);return 1==e.length&&(e="0"+e),1==f.length&&(f="0"+f),1==a.length&&(a="0"+a),"#"+e+f+a},this.getHelpXML=function(){for(var e=new Array,a=0;a<r.length;a++)for(var t=r[a].example,d=0;d<t.length;d++)e[e.length]=t[d];for(var l in f)e[e.length]=l;var n=document.createElement("ul");n.setAttribute("id","rgbcolor-examples");for(var a=0;a<e.length;a++)try{var i=document.createElement("li"),o=new RGBColor(e[a]),s=document.createElement("div");s.style.cssText="margin: 3px; border: 1px solid black; background:"+o.toHex()+"; color:"+o.toHex(),s.appendChild(document.createTextNode("test"));var c=document.createTextNode(" "+e[a]+" -> "+o.toRGB()+" -> "+o.toHex());i.appendChild(s),i.appendChild(c),n.appendChild(i)}catch(b){}return n}}



/**
 * dragscroll
 */

(function() {
	$.fn.dragScroll = function() {
		var target = this;
		$(this).mousedown(function (event) {
			$(this)
				.data('down', true)
				.data('x', event.clientX)
				.data('y', event.clientY)
				.data('scrollLeft', this.scrollLeft)
				.data('scrollTop', this.scrollTop);
			return false;
		}).css({
			'overflow': 'hidden',
			'cursor': 'move'
		});

		$(document).mousemove(function (event) {
			if ($(target).data('down') == true) {
				target.scrollLeft($(target).data('scrollLeft') + $(target).data('x') - event.clientX);
				target.scrollTop($(target).data('scrollTop') + $(target).data('y') - event.clientY);
				return false; // 譁�ｭ怜�驕ｸ謚槭ｒ謚第ｭ｢
			}
		}).mouseup(function (event) {
			$(target).data('down', false);
		});

		return this;
	}
})(jQuery);
