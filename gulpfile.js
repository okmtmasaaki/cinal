"use strict";

/*****************************************************
 *
 *  Gulp [update:2018/1/14]
 *
 *****************************************************/

const IS_DEBUG       = true;
const IS_HTTPS       = false;
const WATCH_INTERVAL = 500;//default:100
const JS_LIB_BUNDLE_NAME = 'libs.js';

const PATHS={
	'src'            :'src/',
	'dist'           :'dist/',
	'src_es'         :'src/_es6/',
	'dist_js'        :'dist/content/dam/shc/jp/cinal-ex/js/',
	//'src_animate'    :'animate/',
	//'src_animate_lib':'animate/common/js/animate/',
	//'dist_animate'   :'dist/common/js/animate/'
};

const JS_LIB_BUNDLE_LIST = [
	//(1A) lib
	//PATHS.src + "_js/lib/createjs.min.js",
	PATHS.src + "_js/lib/jquery-3.1.1.min.js",
	PATHS.src + "_js/lib/plugins.js",
	//PATHS.src + "_js/lib/jquery.mosaic-in-0.5.js",
	//PATHS.src + "_js/lib/matter.min.js",
	//PATHS.src + "_js/lib/pixi.min.js",
	//PATHS.src + "_js/lib/velocity.min.js",
	//PATHS.src + "_js/lib/jquery.scrollify.js",
	//PATHS.src + "_js/lib/slick.min.js",
	//PATHS.src + "_js/lib/jquery.colorbox-min.js",
	PATHS.src + "_js/lib/TweenMax.min.js",
	//PATHS.src + "_js/lib/bezier.js",
	//PATHS.src + "_js/lib/tweakpane.min.js",
	//PATHS.src + "_js/lib/swiper.js",
	//PATHS.src + "_js/lib/TweenMaxPlugins/PixiPlugin.min.js",
	//PATHS.src + "_js/lib/TweenMaxPlugins/CustomEase.min.js",
	//PATHS.src + "_js/lib/pixi.min.js",
	//PATHS.src + "_js/lib/PixiPlugins/pixi-filters.js",

	//(1B) libあまり使わない
	//PATHS.src + "_js/lib/hammer.min.js",
	//PATHS.src + "_js/lib/soundjs.min.js",
	//PATHS.src + "_js/lib/preloadjs.min.js",
	//PATHS.src + "_js/lib/ScrollMagic.min.js",
	//PATHS.src + "_js/lib/debug.addIndicators.min.js",
	//PATHS.src + "_js/lib/ScrollmagicPlugins/jquery.ScrollMagic.min.js",
	//PATHS.src + "_js/lib/ScrollmagicPlugins/animation.gsap.min.js",
	//PATHS.src + "_js/lib/ScrollmagicPlugins/debug.addIndicators.min.js",

	//(2) 生js
	PATHS.src + "_js/ResponsiveSetting.js",
	PATHS.src + "_js/util.js",

	//(3) common
	PATHS.src + "_js/lib_common.js"
];

const ES_LIST = [
	{"src":PATHS.src_es+"Common.es6","dist":PATHS.dist+"content/dam/shc/jp/cinal-ex/js/common.js"},
	{"src":PATHS.src_es+"index.es6","dist":PATHS.dist+"content/dam/shc/jp/cinal-ex/js/index.js"}
];



const gulp			= require('gulp');
const gulpif		= require('gulp-if');
const rename      	= require('gulp-rename');
const plumber     	= require('gulp-plumber');// エラーによる強制停止を防ぐ
const notify      	= require('gulp-notify'); // デスクトップ通知
const sourcemaps  	= require('gulp-sourcemaps');// ソースマップ
const ignore      	= require('gulp-ignore'); // ファイル対象除外
const pug         	= require('gulp-pug');    // Pug
const data        	= require('gulp-data');   // Pug にサイトマップdataを渡す
const htmlhint    	= require('gulp-htmlhint'); // htmlのValidationチェック
const uglify      	= require('gulp-uglify'); // js圧縮
const less        	= require('gulp-less');   // Less
const lessFunctions = require('less-plugin-functions'); // Lessで関数使えるプラグイン
const autoprefixer	= require('gulp-autoprefixer');// プレフィックス自動付与
const concat      	= require('gulp-concat'); // 連結
const cssmin      	= require('gulp-cssmin'); // css圧縮
const browserSync 	= require('browser-sync');// ブラウザ更新
const runSequence 	= require('run-sequence'); // 同期処理
const csv2json    	= require('csvtojson');   // csv to json
const glob 			= require('glob');		 // 正規表現
const del         	= require('del');

// for Babel
const babelify    	= require('babelify');
const browserify  	= require('browserify');
const vBuffer     	= require('vinyl-buffer');
const vSource     	= require('vinyl-source-stream');



//===============================================================
// Task
//===============================================================

gulp.task('default', function(){
	return runSequence(
		'build',
		'watch'
	);
});
gulp.task('dev', ['watch']);
gulp.task('build',function(){
	return runSequence(
		'clean',
		[
			'copy_files'
			//,'copy_animate_js','copy_animate_lib'
		],
		[
			'pug',
			'less',
			'concat_js',
			'babel'
		],
		//'htmlhint',
		'clean_after',
		'browser-sync'
	);
});


const watchPugFiles   = PATHS.src + "**/*.pug";
const watchLessFiles  = PATHS.src + "**/*.less";
const watchJsFiles    = PATHS.src + "**/*.js";
const watchEsFiles    = PATHS.src + "**/*.es6";
//const animateFiles	  = PATHS.src_animate+"**/*";

gulp.task('watch', function(){
	gulp.watch(originalFiles, {interval:WATCH_INTERVAL}, ['copy_files']);
	gulp.watch(watchPugFiles, {interval:WATCH_INTERVAL}, ['pug']);
	gulp.watch(watchLessFiles, {interval:WATCH_INTERVAL}, ['less']);
	gulp.watch(watchJsFiles, {interval:WATCH_INTERVAL}, ['concat_js']);
	gulp.watch(watchEsFiles, {interval:WATCH_INTERVAL}, ['babel']);
	//gulp.watch(animateFiles, {interval:WATCH_INTERVAL}, ['copy_animate_js', 'copy_animate_lib']);
});



//===============================================================
// Method
//===============================================================

//--------------------------------------------------------
// BrowserSync
//--------------------------------------------------------
gulp.task('browser-sync', ()=> {
	browserSync({
		open: true,
		notify: false,
		port:3000,
		https:IS_HTTPS,
		startPath: './top.html',
		open: 'external',
		notify: false,
		server:{
			baseDir:[PATHS.dist],
			routes:{}

		}
	});
});



//--------------------------------------------------------
// Pug
//--------------------------------------------------------
const pugConfig = {
	pretty: true,
	basedir: PATHS.src
}
const pugData = ( file )=> {
	var c, filepath;
	var json = {
		title: '',
		keywords: '',
		description: ''
	};
	if ( file.path.length !== 0 ) {
		// Windowsパスを/区切りに統一
		c = file.path.split('\\').join('/');
		filepath = ( '/' + c.split( 'src/' )[1].replace('.pug', '') );
		// metaデータ追加
		if ( typeof sitemap[ filepath ] != 'undefined' ) {
			json = extend( json, sitemap[ filepath ] );
		}
	}
	return json;
};
const pugFiles = [PATHS.src + "**/*.pug", "!"+PATHS.src+"_**/*.pug"]; //「_」から始まるディレクトリ内は除外
gulp.task('pug', ()=> {
	return gulp.src(pugFiles)
		.pipe(plumber())
		.pipe(data(pugData))
		.pipe(pug(pugConfig))
		.pipe(gulp.dest(PATHS.dist))
		.pipe(browserSync.reload({stream:true}));
});

// サイトマップ取得
// http://qiita.com/yokoh9/items/13f2f601cffbec5ba276
//------------------------------------------------
const csvParseOptions = {};
const sitemap = {};
const getSitemap = ()=>{
	var conv = new csv2json.Converter({});
	var mapjson = conv.fromFile('sitemap.csv', function(err,result){
		result.forEach(function( page ){
			sitemap[ page['path'] ] = {
				title: page['title'],
				keywords: page['keywords'],
				description: page['description']
			};
			//console.log(page);
		})
	});
};
getSitemap();

// object拡張
const extend = (obj1, obj2)=> {
	if( !obj2 ){
		obj2 = {};
	}
	for( var key in obj2 ){
		obj1[ key ] = obj2[ key ];
	}
	return obj1;
};

// htmlhint
// http://bashalog.c-brains.jp/15/10/08-121056.php
// ※構造難有りにより、1週遅れでlintチェックする
//-----------------------------------------------
const htmlFiles = [PATHS.dist + "**/*.html", "!"+PATHS.dist+"_pug/**/*.html"]; //「_pug」ディレクトリを除く
gulp.task('htmlhint', ()=> {
	return gulp.src(htmlFiles)
		.pipe(htmlhint())
		.pipe(htmlhint.failReporter())
});



//--------------------------------------------------------
// Less
//--------------------------------------------------------
const lessFiles = [PATHS.src + "**/!(_)*.less", "!"+PATHS.src+"_**/*.less"]; //「_」から始まるファイルと、「_」から始まるディレクトリ内は除外
gulp.task('less', function() {
	return gulp.src(lessFiles)
		.pipe(plumber({
			errorHandler: notify.onError("Error: <%= error.message %>")
		}))
		//.pipe(sourcemaps.init())
		.pipe(less({plugins:[new lessFunctions]}))
		.pipe(autoprefixer("last 2 versions", "ie >= 9", "Android >= 4", "ios_saf >= 8"))
		//.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(PATHS.dist))
		.pipe(browserSync.reload({stream:true}));
		/*
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(PATHS.dist))
		*/
});



//--------------------------------------------------------
// JS
//--------------------------------------------------------

// 生jsを結合＆最小化
//------------------------------------------------
gulp.task('concat_js', function () {
	return gulp.src(JS_LIB_BUNDLE_LIST)
		.pipe(concat(JS_LIB_BUNDLE_NAME))
		//.pipe(gulp.dest(PATHS.dist_js))// minしてないjs
		.pipe(uglify({preserveComments:'some'})) //「/*!」で始まるコメント・ライセンス表記を残す
		.pipe(rename({suffix:'.min'}))
		.pipe(gulp.dest(PATHS.dist_js));
});


// Babel
//------------------------------------------------
gulp.task("babel", ()=> {
	ES_LIST.forEach(function(item) {
		let fileName = item.dist.replace(/.+\/(.+)\.es6/, '$1');
		browserify(item.src, {debug:true})	// debug:true -> js 末尾に sourcemap を追記
			.transform(babelify, {presets:['es2015','stage-0']}) // http://marunouchi-tech.i-studio.co.jp/2665/
			.bundle()
			.on("error", function (err) { console.log("Error : " + err.message); })
			.pipe(vSource(fileName))
			.pipe(vBuffer())
			.pipe(sourcemaps.init({loadMaps:true}))
			//.pipe(uglify({preserveComments:'some'})) //「/*!」で始まるコメント・ライセンス表記を残す
			//.pipe(rename({suffix:'.min'}))
			.pipe(gulp.dest(''))
			.pipe(sourcemaps.write())
			.pipe(browserSync.reload({stream:true}));
	});
});



//--------------------------------------------------------
// Copy
//--------------------------------------------------------
const originalFiles = [
	PATHS.src + "**/*.*",
	PATHS.src + "**/.htaccess",
	"!" + PATHS.src + "**/*.pug",
	"!" + PATHS.src + "**/*.less",
	"!" + PATHS.src + "**/*.es6",
	"!" + PATHS.src + "_**/*",	//「_」ディレクトリは除外
	"!" + PATHS.src + "_js/**/*", //「_js」ディレクトリ内は除外
	"!" + PATHS.src + "_es6/**/*" //「_es6」ディレクトリ内は除外
];
gulp.task('copy_files',function(){
	return gulp.src(originalFiles)
		.pipe(plumber())
		.pipe(gulp.dest(PATHS.dist));
});
gulp.task('copy_animate_js',function(){
	return gulp.src(PATHS.src_animate+"*.js")
		.pipe(gulp.dest(PATHS.dist_animate))
});
gulp.task('copy_animate_lib',function(){
	return gulp.src(PATHS.src_animate_lib+"**")
		.pipe(gulp.dest(PATHS.dist_animate))
});


// clean 一括削除
//------------------------------------------------
gulp.task('clean',function(){
	return del(PATHS.dist);
});
gulp.task('clean_after',function(){
	return del(PATHS.dist+"_pug");
});

